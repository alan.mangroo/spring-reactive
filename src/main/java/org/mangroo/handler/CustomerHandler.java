package org.mangroo.handler;

import org.mangroo.dao.CustomerDao;
import org.mangroo.dto.Customer;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CustomerHandler {

    private final CustomerDao dao;


    public CustomerHandler(CustomerDao dao) {
        this.dao = dao;
    }

    public Mono<ServerResponse> loadCustomers(ServerRequest serverRequest) {
        Flux<Customer> customerFlux = dao.getCustomersFlux();
        return ServerResponse.ok().body(customerFlux, Customer.class);
    }

    public Mono<ServerResponse> findCustomer(ServerRequest serverRequest) {
        int id = Integer.parseInt(serverRequest.pathVariable("input"));
//        dao.getCustomersFlux().filter(c -> c.getId() == id).take(1).single();
        Mono<Customer> customerMono = dao.getCustomersFlux()
        .filter(c -> c.getId() == id)
        .next();
        return ServerResponse.ok().body(customerMono, Customer.class);
    }

    public Mono<ServerResponse> saveCustomer(ServerRequest serverRequest) {
        Mono<Customer> customerMono = serverRequest.bodyToMono(Customer.class);
        Mono<String> saveResponse = customerMono.map(dto -> dto.getId() + ":" + dto.getName());
        return ServerResponse.ok().body(saveResponse, String.class);
    }

}
